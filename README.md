# അടിപൊളി ബ്രൌസർ എക്സ്റ്റൻഷനുകൾ
## SponsorBlock
SponsorBlock ഒരു ഓപ്പൺ സോഴ്സ് ബ്രൌസർ എക്സ്റ്റൻഷൻ ആണ്. യൂട്യൂബ് വിഡിയോകളിലെ സ്പോൺസർ സന്ദേശങ്ങളും, ഇന്ട്രോകളും, ഔട്രോകളും തടയുന്നൂ.
<br />
**ഡൌൺലോഡ് ചെയ്യുക:**
  <a href="https://chrome.google.com/webstore/detail/mnjggcdmjocbbbhaepdhchncahnbgone">Chrome/Chromium</a> |
  <a href="https://addons.mozilla.org/addon/sponsorblock/?src=external-github">Firefox</a> |
  <a href="https://github.com/ajayyy/SponsorBlock/wiki/Android">Android</a> |
  <a href="https://github.com/ajayyy/SponsorBlock/wiki/Edge">Edge</a> |
  <a href="https://sponsor.ajay.app">Website</a> |
  <a href="https://sponsor.ajay.app/stats">Stats</a>
## uBlock Origin


